//
//  ParkTableViewCell.swift
//  CodeBusters
//
//  Created by Nanda Penumalli on 5/1/18.
//  Copyright © 2018 Nanda Penumalli. All rights reserved.
//

import UIKit

class ParkTableViewCell: UITableViewCell {
    @IBOutlet weak var parkName: UILabel!
    @IBOutlet weak var timings: UILabel!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var parkImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
