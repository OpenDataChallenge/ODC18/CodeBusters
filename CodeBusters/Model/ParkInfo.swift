//
//  ParkInfo.swift
//  CodeBusters
//
//  Created by Nanda Penumalli on 5/1/18.
//  Copyright © 2018 Nanda Penumalli. All rights reserved.
//

import Foundation
import CoreLocation

struct ParkInfo {
    var id: String?
    var address: String?
    var contact: String?
    var amenities: String?
    var activities: String?
    var name: String?
    var timings: String?
    var distance: String?
    var lattitue : CLLocationDegrees?
    var longitude : CLLocationDegrees?
    var description: String?
}
