//
//  MyLocation.swift
//  CodeBusters
//
//  Created by Nanda Penumalli on 5/3/18.
//  Copyright © 2018 Nanda Penumalli. All rights reserved.
//

import Foundation
import CoreLocation

class MyLocation {
    static let sharedInstance = MyLocation()
    var currentLocation : CLLocationCoordinate2D!
    var parksArray : [ParkInfo] = []
}
