//
//  ViewController.swift
//  CodeBusters
//
//  Created by Nanda Penumalli on 4/15/18.
//  Copyright © 2018 Nanda Penumalli. All rights reserved.
//

import UIKit


class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var menuTableView: UITableView!
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var trailingConstraint: NSLayoutConstraint!
    let menuIcons:[String] = ["homeIcon","alerts","emergencyContact","help"]
    let menuTitles:[String] = ["Home","Notifications","Contact Us","Help"]
    var menuVisible: Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
       
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func menuButtonClicked(_ sender: Any) {
        if !menuVisible {
            leadingConstraint.constant = self.view.frame.size.width - 100
            trailingConstraint.constant = self.view.frame.size.width + 100
            menuVisible = true
        } else {
            leadingConstraint.constant = 0
            trailingConstraint.constant = 0
            menuVisible = false
        }
        UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseIn, animations: {
            self.view.layoutIfNeeded()
        }) { (animated) in
            print("animation done")
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menuTitles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MenuItemTableViewCell
        let imageName = self.menuIcons[indexPath.row]
        
        cell.menuItemIcon.image = UIImage(named: imageName)
        cell.menuItemTitle.text = menuTitles[indexPath.row]
        return cell
    }
}

