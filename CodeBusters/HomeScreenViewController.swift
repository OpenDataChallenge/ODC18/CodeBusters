//
//  HomeScreenViewController.swift
//  CodeBusters
//
//  Created by Nanda Penumalli on 5/1/18.
//  Copyright © 2018 Nanda Penumalli. All rights reserved.
//

import UIKit
import CoreLocation

class HomeScreenViewController: UIViewController,UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var parksTableView: UITableView!
    var parksArray:  [ParkInfo]!
    var images:[UIImage]?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        parksArray = []
        images = [UIImage(named: "img1.jpg"),UIImage(named: "img2.jpg"),UIImage(named: "img3.jpg"),UIImage(named: "img4.jpg"),UIImage(named: "img5.jpg"),UIImage(named: "img6.jpg"),UIImage(named: "img7.jpg"),UIImage(named: "img8.jpg"),UIImage(named: "img9.jpg"),UIImage(named: "img10.jpg"),UIImage(named: "img11.jpg"),UIImage(named: "img12.jpg"),UIImage(named: "img13.jpg"),UIImage(named: "img14.jpg"),UIImage(named: "img15.jpg"),UIImage(named: "img16.jpg")] as? [UIImage]
        self.parksTableView.register(UINib(nibName: "ParkTableViewCell", bundle: nil), forCellReuseIdentifier: "ParkTableViewCell")
        self.fetchParks()
        // Do any additional setup after loading the view.
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavBar()
    }
    
    private func updateNavBar(home isHomeScreen:Bool = true) {
        if isHomeScreen {
            self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
            self.navigationController?.navigationBar.shadowImage = nil
        }
        else {
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.isTranslucent = true
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.parksArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ParkTableViewCell", for: indexPath) as! ParkTableViewCell
        let park = self.parksArray[indexPath.row]
        
        cell.parkName.text = park.name
        cell.distance.text = park.distance
        cell.timings.text = park.contact
        cell.parkImageView.image = self.images?[indexPath.row]
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller: ParkDetailsViewController = storyboard.instantiateViewController(withIdentifier: "ParkDetailsViewController") as! ParkDetailsViewController
        controller.park = self.parksArray[indexPath.row]
        controller.image = self.images![indexPath.row]
        self.navigationController?.pushViewController(controller, animated: true)
        tableView.deselectRow(at: indexPath, animated: false)
    }

    func fetchParks() {
        DispatchQueue.global(qos: .userInitiated).async{ [weak self] in
            let path = Bundle.main.path(forResource: "Parks_Code_Busters", ofType: "json")
            let data:NSData = try! NSData(contentsOfFile: path!)
            do {
                guard let nsArrayObject = try JSONSerialization.jsonObject(with: data as Data, options: .allowFragments) as? [[String:String]] else {
                    return
                }
                
                for parkDictionary in nsArrayObject {
                    var park = ParkInfo()
                    park.name = parkDictionary["Park_Name"]
                    park.lattitue = Double(parkDictionary["Park_Lat"] ?? "")
                    park.longitude = Double(parkDictionary["Park_Lon"] ?? "")
                    park.id = parkDictionary["ID"]
                    park.description = parkDictionary["Park_Overview"]
                    park.contact = parkDictionary["Park_Contact"]
                    park.address = parkDictionary["Park_Address"]
                    park.amenities = parkDictionary["Park_Amenities"]
                    park.activities = parkDictionary["Park_Activities"]
                    
                    if let location = MyLocation.sharedInstance.currentLocation {
                        park.distance = self?.getDistanceInMiles(lat: park.lattitue, long: park.longitude, cuurentLocation: location)
                    }
                    else {
                        park.distance = "Distance not available"
                    }
                    
                    self?.parksArray.append(park)
                }
                if let array = self?.parksArray {
                    MyLocation.sharedInstance.parksArray = array
                }
                DispatchQueue.main.async {
                    self?.parksArray = self?.parksArray.sorted(by: { ($0.distance! as NSString).doubleValue < ($1.distance! as NSString).doubleValue })
                    self?.parksTableView.reloadData()
                }

            }catch {
                print("Error: \(error.localizedDescription)")
            }
            
        }
        
        
    }
    
    func getDistanceInMiles(lat: CLLocationDegrees!, long: CLLocationDegrees!, cuurentLocation: CLLocationCoordinate2D!) -> String {
        let loc = CLLocation(latitude: lat, longitude: long)
        let currentLoc = CLLocation(latitude: cuurentLocation.latitude, longitude: cuurentLocation.longitude)
        
        let distanceInMeters = loc.distance(from: currentLoc)/1000.0
        return String(format: "%.2f", distanceInMeters*0.000621371) + " Miles away from your location"
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
