//
//  ParkDetailsViewController.swift
//  CodeBusters
//
//  Created by Nanda Penumalli on 5/4/18.
//  Copyright © 2018 Nanda Penumalli. All rights reserved.
//

import UIKit

class ParkDetailsViewController: UIViewController {

    @IBOutlet weak var descTextView: UITextView!
    @IBOutlet weak var amenitiesLabel: UILabel!
    @IBOutlet weak var addressTextView: UITextView!
    var image:UIImage!
    
    @IBOutlet weak var parkImage: UIImageView!
    
    @IBOutlet weak var activitiesLabel: UILabel!
    var park: ParkInfo! = ParkInfo()
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
//        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]

        // Do any additional setup after loading the view.
        
    }

    func loadData() {
        parkImage.image = image
        descTextView.text = self.park.description
        amenitiesLabel.text = self.park.amenities
        addressTextView.text = "\(self.park.address ?? "")\n\(self.park.contact ?? "")"
        activitiesLabel.text = self.park.activities
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
