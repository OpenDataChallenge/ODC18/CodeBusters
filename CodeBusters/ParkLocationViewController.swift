//
//  ParkLocationViewController.swift
//  CodeBusters
//
//  Created by Nanda Penumalli on 5/4/18.
//  Copyright © 2018 Nanda Penumalli. All rights reserved.
//

import UIKit
import MapKit

class ParkLocationViewController: UIViewController,MKMapViewDelegate {
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.showsUserLocation = true
        addPin()
        
        let viewRegion = MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake(39.6837, -75.7497), 200000, 200000)
        self.mapView.setRegion(viewRegion, animated: false)
        
        // Do any additional setup after loading the view.
    }

    private func addPin() {
        for park in MyLocation.sharedInstance.parksArray where (park.lattitue != nil && park.longitude != nil) {
            let annotation = MKPointAnnotation()
            annotation.coordinate = CLLocationCoordinate2DMake(park.lattitue!, park.longitude!)
            mapView.addAnnotation(annotation)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
